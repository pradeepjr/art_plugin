﻿using HHDev.Core.WPF.Converters;
using HHDev.DataManagement.Desktop.Definitions.Displays;
using HHDev.DataManagement.Desktop.Definitions.PluginFramework;
using ART.HHDM.DesktopPlugin.PlannedRun;
using ART.HHDM.DesktopPlugin.PressureCalculation;
using ART.HHDM.DesktopPlugin.RunSheet;
using ART.HHDM.DesktopPlugin.SetupSheet;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Reflection;
using ART.HHDM.DesktopPlugin.TyreSpec;
using HHDev.ReportGeneration.HtmlReportingV2;
using HHDev.DataManagement.Desktop.Definitions.ExcelExport;

namespace ART.HHDM.DesktopPlugin
{
    public class HhdmPlugin : IHhDataManagementPlugin
    {
        private List<KeyValuePair<Guid, CarRunStructure>> _carRunStructures;
        private List<KeyValuePair<Guid, CarSetupStructure>> _carSetupStructures;
        private List<KeyValuePair<Guid, PressureCalculationCustomizationConfig>> _pressureCalcCustomization;
        private List<KeyValuePair<Guid, PlannedRunCustomizationConfig>> _plannedRunCustomConfig;
        private List<KeyValuePair<Guid, LapCustomizationConfig>> _lapCustomizationConfigs;

        public string Name => "ART HHDM Plugin";
        public Guid PluginId => Guid.Parse("47c1bcad-011e-4766-86cb-b28fb86682ee");
        public List<KeyValuePair<Guid, CarSetupStructure>> CarSetupStructures => _carSetupStructures;
        public List<KeyValuePair<Guid, PressureCalculationCustomizationConfig>> PressureCalculationCustomConfigs => _pressureCalcCustomization;

        public List<KeyValuePair<Guid, PlannedRunCustomizationConfig>> PlannedRunCustomConfigs => _plannedRunCustomConfig;

        public List<KeyValuePair<Guid, CarRunStructure>> CarRunSheetStructures => _carRunStructures;

        private List<KeyValuePair<Guid, List<IssueCategory>>> _accountIssueCategories;
        public List<KeyValuePair<Guid, List<IssueCategory>>> AccountIssueCategories => _accountIssueCategories;

        public Guid[] AccountIds => new Guid[] { Guid.Parse("bced4171-7f69-4393-ae19-b9b5a5137ff5") };

        private List<KeyValuePair<Guid, TyreSpecificationCustomizationConfig>> _tyreSpecificationCustomConfigs;
        public List<KeyValuePair<Guid, TyreSpecificationCustomizationConfig>> TyreSpecificationCustomConfigs => _tyreSpecificationCustomConfigs;

        public BitmapImage SplashscreenImage => null;

        public ImageSource SoftwareIcon => null;

        public string SoftwareName => null;

        public string AutoUpdateKey => "ART_HHDM_DesktopPlugin";

        public string VersionNumber => Assembly.GetExecutingAssembly().GetName().Version.ToString();

        public HhdmPlugin()
        {
            _carRunStructures = new List<KeyValuePair<Guid, CarRunStructure>>()
            {
                new KeyValuePair<Guid, CarRunStructure>(Guid.Parse("253dd637-be66-4b0e-beaf-cbf88c27c9b8"),
                                                        new CarRunStructure((w) => new ARTFlatRunSheetModel(w), typeof(RunSheetRightPanel),
                                                        new List<HHColumnDefinition>()
                                                        {
                                                            new HHColumnDefinition() {ColumnTitle = "Fuel Rem.",  BindingPath="FuelRemaining", ColumnDefaultWidth=64},
                                                            new HHColumnDefinition() {ColumnTitle = "Fuel Cons.",  BindingPath="FuelConsumed", ColumnDefaultWidth=64},
                                                            new HHColumnDefinition() {ColumnTitle = "Max Speed",  BindingPath="Max_Speed", ColumnDefaultWidth=64},
                                                            new HHColumnDefinition() {ColumnTitle = "Possible Laps",  BindingPath="LapsPossible", ColumnDefaultWidth=64},
                                                            new HHColumnDefinition() {ColumnTitle = "Remaining Fuel",  BindingPath="RemainingFuel", ColumnDefaultWidth=64},
                                                            new HHColumnDefinition() {ColumnTitle = "Fuel cons. needed",  BindingPath="NeededFuelCons", ColumnDefaultWidth=64},
                                                            new HHColumnDefinition() {ColumnTitle = "Target Fuel cons.",  BindingPath="TargetFuelCons", ColumnDefaultWidth=64},
                                                        }, null))
            };

            _carSetupStructures = new List<KeyValuePair<Guid, CarSetupStructure>>
            {
                new KeyValuePair<Guid, CarSetupStructure>(Guid.Parse("c25767c2-b82d-4308-aa7f-06fd1005ccf5"),
                                                                              new CarSetupStructure((x) => new ARTFlatSetupSheetModel(x),
                                                                              typeof(ARTSetupSheetLabelsView),
                                                                              typeof(ARTSetupSheetEditorView),
                                                                              null, null, (x,y) => ARTFlatSetupSheetModel.ExportSetups(x,y)))
            };

            _pressureCalcCustomization = new List<KeyValuePair<Guid, PressureCalculationCustomizationConfig>>()
            {
                new KeyValuePair<Guid, PressureCalculationCustomizationConfig>(Guid.Parse("6dbd74bb-ac2b-4f9e-8144-0882077a87a8"),
                                                                               new PressureCalculationCustomizationConfig((x,y,z) => new ARTPressureCalculationFlatModel(x,y,z),
                                                                               typeof(ARTPressureCalculationView),null))
            };

            _plannedRunCustomConfig = new List<KeyValuePair<Guid, PlannedRunCustomizationConfig>>()
            {
                new KeyValuePair<Guid, PlannedRunCustomizationConfig>(Guid.Parse("a77507dc-6aa5-49e2-ac5f-ef97ad96fb29"),
                new PlannedRunCustomizationConfig((w, x, y, z, xx) => new FlatPlannedRunModel(w, x, y, z, xx), GetPlannedRunColumns, new PlannedRunUpdater()))
            };

            _tyreSpecificationCustomConfigs = new List<KeyValuePair<Guid, TyreSpecificationCustomizationConfig>>()
            {
                new KeyValuePair<Guid, TyreSpecificationCustomizationConfig>(Guid.Parse("999860ab-2e39-40c9-935d-e89a734200f6"),
                new TyreSpecificationCustomizationConfig((w, x, y) => new FlatTyreSpecificationModel(w, x), null))
            };

            _accountIssueCategories = new List<KeyValuePair<Guid, List<IssueCategory>>>()
            {
                new KeyValuePair<Guid, List<IssueCategory>>(Guid.Parse("bced4171-7f69-4393-ae19-b9b5a5137ff5"),
                    new List<IssueCategory>()
                    {
                        new IssueCategory("d198762d-bfad-4cba-a107-3bd031b36222","CAT1"),
                        new IssueCategory("1cd48785-9db1-42b1-aeaa-7ca995653ee4","CAT2"),
                        new IssueCategory("4d191f6e-63b9-4b45-a3e7-80d6768cdde5","CAT3"),
                    })
            };

            _lapCustomizationConfigs = new List<KeyValuePair<Guid, LapCustomizationConfig>>()
            {
                new KeyValuePair<Guid, LapCustomizationConfig>(Guid.Parse("20392160-2cb4-4680-945a-6c3b5e98bd64"),  new LapCustomizationConfig((x) => new ARTFlatLapModel(x)))
            };

        }
        public static IHTMLReportBuilder HTMLReportBuilder { get; set; }

        public List<KeyValuePair<Guid, LapCustomizationConfig>> LapCustomizationConfigs => _lapCustomizationConfigs;

        public void InitializeReporting(IHTMLReportBuilder anHtmlReportBuilder)
        {
            HTMLReportBuilder = anHtmlReportBuilder;
        }

        public ObservableCollection<DataGridColumn> GetPlannedRunColumns(ResourceDictionary aResourceDictionary)
        {
            var plannedRunColumns = new ObservableCollection<DataGridColumn>();

            plannedRunColumns.Add(new DataGridTextColumn()
            {
                Binding = new Binding("Doubles.OutingLaps")
                {
                },
                Header = "Outing",
                IsReadOnly = true,
                Width = 75,
            });

            plannedRunColumns.Add(new DataGridTextColumn()
            {
                Binding = new Binding("Doubles.TotalLaps")
                {
                },
                Header = "Total Laps",
                Width = 75,
            });

            var driverItemsSourceBinding = new Binding("DataContext.Drivers")
            {
                Source = aResourceDictionary["ProxyElement"],
            };

            var driverColumn = new DataGridComboBoxColumn()
            {
                Header = "Driver",
                SelectedItemBinding = new Binding("Driver"),
                Width = 75,
            };
                       

            BindingOperations.SetBinding(driverColumn, DataGridComboBoxColumn.ItemsSourceProperty, driverItemsSourceBinding);

            plannedRunColumns.Add(driverColumn);

            var tyreSetsItemSourceBinding = new Binding("DataContext.TyreSets")
            {
                Source = aResourceDictionary["ProxyElement"],
                UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
            };

            var tyreSetColumn = new DataGridComboBoxColumn()
            {
                Header = "Tyre Set",
                SelectedItemBinding = new Binding("TyreSet"),
                DisplayMemberPath = "Name",
                Width = 75,
            };

            BindingOperations.SetBinding(tyreSetColumn, DataGridComboBoxColumn.ItemsSourceProperty, tyreSetsItemSourceBinding);

            plannedRunColumns.Add(tyreSetColumn);

            plannedRunColumns.Add(new DataGridTextColumn()
            {
                Binding = new Binding("Strings.Description"),
                Header = "Notes",
                Width = 400,
            });

            plannedRunColumns.Add(new DataGridTextColumn()
            {
                Binding = new Binding("Doubles.PitTime")
                {
                    Converter = new LapTimeDisplayConverter()
                },
                Header = "Pit Time",
                Width = 75,
            });

            plannedRunColumns.Add(new DataGridTextColumn()
            {
                Binding = new Binding("DateTimes.StartTime")
                {
                    StringFormat = "HH:mm:ss"
                },
                Header = "Start Time",
                IsReadOnly = true,
                Width = 75,
            });

            plannedRunColumns.Add(new DataGridTextColumn()
            {
                Binding = new Binding("DateTimes.EndTime")
                {
                    StringFormat = "HH:mm:ss"
                },
                Header = "End Time",
                IsReadOnly = true,
                Width = 75,
            });

            var ovenTimeTemplate = new DataGridTemplateColumn()
            {
                Header = "Oven Time",
            };

            //Binding b1 = new Binding("SelectedDate");
            //b1.Mode = BindingMode.TwoWay;

            //FrameworkElementFactory factory1 = new FrameworkElementFactory(typeof(DatePicker));
            //factory1.SetValue(DatePicker.SelectedDateProperty, b1);

            //DataTemplate cellTemplate1 = new DataTemplate();
            //cellTemplate1.VisualTree = factory1;
            //ovenTimeTemplate.CellTemplate = cellTemplate1;

            //plannedRunColumns.Add(ovenTimeTemplate);

            plannedRunColumns.Add(new DataGridTextColumn()
            {
                Binding = new Binding("Doubles.FuelStart")
                {
                    StringFormat = "N2"
                },
                Header = "Fuel Start",
                Width = 75,
            });

            plannedRunColumns.Add(new DataGridTextColumn()
            {
                Binding = new Binding("Doubles.FuelEnd")
                {
                    StringFormat = "N2"
                },
                Header = "Fuel End",
                IsReadOnly = true,
                Width = 75,
            });

            plannedRunColumns.Add(new DataGridTextColumn()
            {
                Binding = new Binding("Doubles.FuelAdded")
                {
                    StringFormat = "N2"
                },
                Header = "Fuel Added",
                Width = 75,
            });


            plannedRunColumns.Add(new DataGridTextColumn()
            {
                Binding = new Binding("Strings.ChangesForNextRun")
                {
                },
                Header = "Changes For Next Run",
                Width = 400,
            });


            return plannedRunColumns;
        }

        public void InitializeExcelExportService(IExcelService anExcelService)
        {
           
        }
    }
}
