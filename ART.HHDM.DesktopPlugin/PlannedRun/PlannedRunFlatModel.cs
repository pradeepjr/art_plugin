﻿using HHDev.DataManagement.Definitions.Entities;
using HHDev.DataManagement.Definitions.Models;
using HHDev.DataManagement.Desktop.Definitions.Displays;
using System.ComponentModel;

namespace ART.HHDM.DesktopPlugin.PlannedRun
{
    public class PlannedRunModel : FlatPlannedRunModel
    {
        public PlannedRunModel(HHDev.DataManagement.Definitions.Entities.PlannedRun anEntity,
                                CustomPropertyDefinition aDefinition,
                                string aTimeZone, BindingList<FlatTyreSetModel> aTyreSets,
             BindingList<Driver> aDrivers)
            : base(anEntity, aDefinition, aTimeZone, aTyreSets, aDrivers)
        {
        }
    }
}
