﻿using HHDev.Core.NETStandard.CustomProperties;
using HHDev.Core.NETStandard.Enums;
using HHDev.Core.NETStandard.Extensions;
using HHDev.Core.NETStandard.Src;
using HHDev.DataManagement.Definitions.Entities;
using HHDev.DataManagement.Desktop.Definitions.Displays;
using HHDev.DataManagement.Desktop.Definitions.PluginFramework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ART.HHDM.DesktopPlugin.PlannedRun
{
    public class PlannedRunUpdater : IPlannedRunUpdater
    {

        public List<IPlannedRunUpdate> RunAdded(FlatPlannedRunModel runAdded, HHBindingList<FlatPlannedRunModel> allPlannedRuns, EventObject anEventObject, SessionObject aSessionObject)
        {
            var updates = new List<IPlannedRunUpdate>();

            // todo, be smarted about lap insert to only recalculate what is necessary
            updates.AddRange(RecalculateStartEndTimes(allPlannedRuns, anEventObject, aSessionObject));
            updates.AddRange(RecalculateFuel(allPlannedRuns, anEventObject.AvgFuelCons));

            return updates;
        }

        public List<IPlannedRunUpdate> RunUpdated(FlatPlannedRunModel modifiedRun, string aPropertyName, HHBindingList<FlatPlannedRunModel> allPlannedRuns, EventObject anEventObject, SessionObject aSessionObject)
        {
            // todo, use prop name to only recalculate when necessary

            var updates = new List<IPlannedRunUpdate>();

            updates.AddRange(RecalculateStartEndTimes(allPlannedRuns, anEventObject, aSessionObject));
            updates.AddRange(RecalculateFuel(allPlannedRuns, anEventObject.AvgFuelCons));

            return updates;
        }

        List<IPlannedRunUpdate> RecalculateFuel(HHBindingList<FlatPlannedRunModel> allPlannedRuns, double? avgFuelCons)
        {
            var updates = new List<IPlannedRunUpdate>();

            if (allPlannedRuns == null || allPlannedRuns.Count == 0)
                return updates;

            var orderedRuns = allPlannedRuns.OrderBy(p => p.Order).ToList();

            double? startingFuel = null;

            for (var i = 0; i < orderedRuns.Count(); i++)
            {
                var dateTimes = orderedRuns[i].DateTimes;
                var doubles = orderedRuns[i].Doubles;

                double? prevRunFuelAdded = null;
                double? prevRunEndFuel = null;

                var totalLaps = orderedRuns[i].TotalLaps;

                if (i == 0)
                {
                    startingFuel = doubles.GetPropertyValue("FuelStart");

                    if (startingFuel == null)
                        return updates;

                    var tmp = CalculateExpectedEndFuel(startingFuel, totalLaps, avgFuelCons, 0);

                    if (tmp != doubles.GetPropertyValue("FuelEnd"))
                    {
                        doubles.SetPropertyValue("FuelEnd", tmp);

                        updates.Add(new PlannedRunUpdate(orderedRuns[i].Id, "FuelEnd", tmp));
                    }

                    continue;
                }

                prevRunFuelAdded = orderedRuns[i - 1].Doubles.GetPropertyValue("FuelAdded");
                prevRunEndFuel = orderedRuns[i - 1].Doubles.GetPropertyValue("FuelEnd");

                if (prevRunEndFuel == null)
                    continue;

                if (prevRunFuelAdded != null)
                    prevRunEndFuel += prevRunFuelAdded;

                if (prevRunEndFuel != doubles.GetPropertyValue("FuelStart"))
                {
                    doubles.SetPropertyValue("FuelStart", prevRunEndFuel);
                    updates.Add(new PlannedRunUpdate(orderedRuns[i].Id, "FuelStart", prevRunEndFuel));
                }

                var expectedEndFuel = CalculateExpectedEndFuel(prevRunEndFuel, totalLaps, avgFuelCons, 0);

                if (expectedEndFuel != doubles.GetPropertyValue("FuelEnd") && doubles.GetPropertyValue("FuelStart") != null)
                {
                    doubles.SetPropertyValue("FuelEnd", expectedEndFuel);
                    updates.Add(new PlannedRunUpdate(orderedRuns[i].Id, "FuelEnd", expectedEndFuel));
                }
            }

            return updates;
        }

        double? CalculateExpectedEndFuel(double? startFuel, double? laps, double? avgFuelCons, double? prevRunFuelAdd)
        {
            if (startFuel == null)
                return null;

            var expectedFuel = startFuel;

            if (laps != null && avgFuelCons != null)
                expectedFuel -= (laps * avgFuelCons);

            if (prevRunFuelAdd != null)
                expectedFuel += prevRunFuelAdd;

            return expectedFuel;
        }

        List<IPlannedRunUpdate> RecalculateStartEndTimes(HHBindingList<FlatPlannedRunModel> allPlannedRuns, EventObject anEventObject, SessionObject aSessionObject)
        {
            var updates = new List<IPlannedRunUpdate>();

            if (allPlannedRuns == null || allPlannedRuns.Count == 0 || anEventObject.AvgLapTime == null)
                return updates;

            var orderedRuns = allPlannedRuns.OrderBy(p => p.Order).ToList();

            for (var i = 0; i < orderedRuns.Count(); i++)
            {
                var dateTimes = orderedRuns[i].DateTimes;
                var doubles = orderedRuns[i].Doubles;

                if (i == 0)
                {
                    var firstStopTime = doubles.GetPropertyValue("PitTime");
                    if (firstStopTime == null)
                        firstStopTime = 0;

                    var sessionStartTime = new DateTime(aSessionObject.Session.StartTime.Ticks, DateTimeKind.Utc);

                    if (dateTimes.GetPropertyValue("StartTime") != sessionStartTime.AddSeconds(firstStopTime.Value))
                    {
                        dateTimes.SetPropertyValue("StartTime", sessionStartTime.AddSeconds(firstStopTime.Value));
                        updates.Add(new PlannedRunUpdate(orderedRuns[i].Id, "StartTime", sessionStartTime.AddSeconds(firstStopTime.Value)));
                    }

                    UpdateEndTime(orderedRuns[i], anEventObject.AvgLapTime, updates, anEventObject);
                    continue;
                }

                var prevRun = orderedRuns[i - 1];
                var prevRunEndTime = prevRun.DateTimes.GetPropertyValue("EndTime");

                var actualStartTime = dateTimes.GetPropertyValue("StartTime");
                var pitTime = doubles.GetPropertyValue("PitTime");

                DateTime? expectedStartTime = prevRunEndTime;

                if (expectedStartTime == null)
                    continue;

                if (pitTime != null)
                    expectedStartTime = expectedStartTime.Value.AddSeconds(pitTime.Value);

                if (actualStartTime != expectedStartTime)
                {
                    dateTimes.SetPropertyValue("StartTime", expectedStartTime);
                    updates.Add(new PlannedRunUpdate(orderedRuns[i].Id, "StartTime", DateTimeHelpers.ConvertDateTimeToUTC(expectedStartTime.Value, anEventObject.Event.TimeZone)));
                }

                UpdateEndTime(orderedRuns[i], anEventObject.AvgLapTime, updates, anEventObject);
            }

            return updates;
        }

        private void UpdateEndTime(FlatPlannedRunModel aPlannedRun, double? anAverageLapTime, List<IPlannedRunUpdate> updates, EventObject anEventObject)
        {
            var calculatedEndTime = CalculateEndTime(aPlannedRun, anAverageLapTime);
            var actualEndTime = aPlannedRun.DateTimes.GetPropertyValue("EndTime");

            if (calculatedEndTime != null && calculatedEndTime != actualEndTime)
            {
                aPlannedRun.DateTimes.SetPropertyValue("EndTime", calculatedEndTime);
                updates.Add(new PlannedRunUpdate(aPlannedRun.Id, "EndTime", DateTimeHelpers.ConvertDateTimeToUTC(calculatedEndTime.Value, anEventObject.Event.TimeZone)));
            }
        }

        private DateTime? CalculateEndTime(FlatPlannedRunModel aPlannedRun, double? anAverageLapTime)
        {
            var startTime = aPlannedRun.DateTimes.GetPropertyValue("StartTime");
            var totalLaps = aPlannedRun.TotalLaps;

            if (anAverageLapTime == null || startTime == null || totalLaps == null)
                return null;

            var pitTime = aPlannedRun.Doubles.GetPropertyValue("PitTime");

            var expectedEndTime = startTime.Value.AddSeconds(anAverageLapTime.Value * totalLaps.Value);

            return expectedEndTime;
        }
    }
}
