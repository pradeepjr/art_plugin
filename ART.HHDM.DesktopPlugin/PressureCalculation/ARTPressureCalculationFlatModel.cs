﻿using HHDev.Core.NETStandard.Src;
using HHDev.DataManagement.Definitions.Caches;
using HHDev.DataManagement.Definitions.Entities;
using HHDev.DataManagement.Definitions.Models;
using HHDev.DataManagement.Desktop.Definitions.Displays;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ART.HHDM.DesktopPlugin.PressureCalculation
{
    public class ARTPressureCalculationFlatModel : FlatPressureCalculationModel
    {
        private const double REFERENCE_RIM_TEMP = 20.0;
        private const double REFERENCE_TRACK_TEMP = 20.0;
        private const double TRACK_TEMP_COEF = 0.005;
        private const double RIM_TEMP_COEF = 0.011;
        private const double RIM_TEMP_COEF_Ax = 0.0041149;
        private const double RIM_TEMP_COEF_B = 0.0060348;
        private const double HOT_to_COLD_COEF = 0.68;
        private const double HOT_BLEED_ERROR = 0.1;
        private const double TIRE_RUNNING_TEMP = 90;

        public ICommand RemoveCalculationCommand { get; set; }

        public ARTPressureCalculationFlatModel(HHDev.DataManagement.Definitions.Entities.PressureCalculation anEntity,
                                               CustomPropertyDefinition aDefinition,
                                              BindingList<FlatTyreSetModel> aTyreSets) :
           base(anEntity, aDefinition, aTyreSets)
        {
            RemoveCalculationCommand = new DelegateCommand(RemoveCalculation);

            base.RegisterParameterAndFunction(new string[] { "FLReferencePressure", "ColdTrackTemp", "ColdRimTemp" },
                                              UpdateFLColdPressure, new string[] { nameof(FLColdPressure) });
            base.RegisterParameterAndFunction(new string[] { "FRReferencePressure", "ColdTrackTemp", "ColdRimTemp" },
                                              UpdateFRColdPressure, new string[] { nameof(FRColdPressure) });
            base.RegisterParameterAndFunction(new string[] { "RLReferencePressure", "ColdTrackTemp", "ColdRimTemp" },
                                              UpdateRLColdPressure, new string[] { nameof(RLColdPressure) });
            base.RegisterParameterAndFunction(new string[] { "RRReferencePressure", "ColdTrackTemp", "ColdRimTemp" },
                                              UpdateRRColdPressure, new string[] { nameof(RRColdPressure) });

            base.RegisterParameterAndFunction(new string[] { "FLReferencePressure", "ColdRimTemp", "FLRimTemperature", "FLOutofBlanketsPressure" },
                                              UpdateFLCheckColdPressure, new string[] { nameof(FLCheckColdPressure) });
            base.RegisterParameterAndFunction(new string[] { "FRReferencePressure", "ColdRimTemp", "FRRimTemperature", "FROutofBlanketsPressure" },
                                              UpdateFRCheckColdPressure, new string[] { nameof(FRCheckColdPressure) });
            base.RegisterParameterAndFunction(new string[] { "RLReferencePressure", "ColdRimTemp", "RLRimTemperature", "RLOutofBlanketsPressure" },
                                              UpdateRLCheckColdPressure, new string[] { nameof(RLCheckColdPressure) });
            base.RegisterParameterAndFunction(new string[] { "RRReferencePressure", "ColdRimTemp", "RRRimTemperature", "RROutofBlanketsPressure" },
                                              UpdateRRCheckColdPressure, new string[] { nameof(RRCheckColdPressure) });

            base.RegisterParameterAndFunction(new string[] { "ColdTrackTemp", "ActualTrackTemp" },
                                  UpdateDeltaPressure, new string[] { nameof(DeltaPressure) });

            base.RegisterParameterAndFunction(new string[] { "ColdTrackTemp", "ActualTrackTemp", "FLOutofBlanketsPressure", "FLHotBleed"},
                                  UpdateFLBlktsandBleedPressure, new string[] { nameof(FLBlktsandBleedPressure) });
            base.RegisterParameterAndFunction(new string[] { "ColdTrackTemp", "ActualTrackTemp", "FROutofBlanketsPressure", "FRHotBleed" },
                                 UpdateFRBlktsandBleedPressure, new string[] { nameof(FRBlktsandBleedPressure) });
            base.RegisterParameterAndFunction(new string[] { "ColdTrackTemp", "ActualTrackTemp", "RLOutofBlanketsPressure", "RLHotBleed" },
                                 UpdateRLBlktsandBleedPressure, new string[] { nameof(RLBlktsandBleedPressure) });
            base.RegisterParameterAndFunction(new string[] { "ColdTrackTemp", "ActualTrackTemp", "RROutofBlanketsPressure", "RRHotBleed" },
                                 UpdateRRBlktsandBleedPressure, new string[] { nameof(RRBlktsandBleedPressure) });

            base.RegisterParameterAndFunction(new string[] { "FLOutofBlanketsPressure", "FLHotBleed","FLReferencePressure", "FLRimTemperature", "ActualTrackTemp" },
                                  UpdateFLRefBleedPressure, new string[] { nameof(FLRefBleedPressure) });
            base.RegisterParameterAndFunction(new string[] { "FROutofBlanketsPressure", "FRHotBleed", "FRReferencePressure", "FRRimTemperature", "ActualTrackTemp" },
                                  UpdateFRRefBleedPressure, new string[] { nameof(FRRefBleedPressure) });
            base.RegisterParameterAndFunction(new string[] { "RLOutofBlanketsPressure", "RLHotBleed", "RLReferencePressure", "RLRimTemperature", "ActualTrackTemp" },
                                  UpdateRLRefBleedPressure, new string[] { nameof(RLRefBleedPressure) });
            base.RegisterParameterAndFunction(new string[] { "RROutofBlanketsPressure", "RRHotBleed", "RRReferencePressure", "RRRimTemperature", "ActualTrackTemp" },
                                  UpdateRRRefBleedPressure, new string[] { nameof(RRRefBleedPressure) });

            base.RegisterParameterAndFunction(new string[] { "FLReferencePressure", "FrontTargetPressure", "FLHotBleed", "FLHotPressure" },
                                 UpdateFLNewRefPressure, new string[] { nameof(FLNewRefPressure) });
            base.RegisterParameterAndFunction(new string[] { "FRReferencePressure", "FrontTargetPressure", "FRHotBleed", "FRHotPressure" },
                                UpdateFRNewRefPressure, new string[] { nameof(FRNewRefPressure) });
            base.RegisterParameterAndFunction(new string[] { "RLReferencePressure", "RearTargetPressure", "RLHotBleed", "RLHotPressure" },
                                UpdateRLNewRefPressure, new string[] { nameof(RLNewRefPressure) });
            base.RegisterParameterAndFunction(new string[] { "RRReferencePressure", "RearTargetPressure", "RRHotBleed", "RRHotPressure" },
                                UpdateRRNewRefPressure, new string[] { nameof(RRNewRefPressure) });

        }

        private void RemoveCalculation(object obj)
        {
            base.RaiseRemovePressureCalculationEvent();
        }
           
        
        #region Properties
        double? _deltaPressure;
        public double? DeltaPressure
        {
            get
            {
                return _deltaPressure;
            }
            set
            {
                if (_deltaPressure == value)
                {
                    return;
                }

                _deltaPressure = value;
                OnPropertyChanged();
            }
        }

        double? _fLColdPressure;
        public double? FLColdPressure
        {
            get
            {
                return _fLColdPressure;
            }
            set
            {
                if (_fLColdPressure == value)
                {
                    return;
                }

                _fLColdPressure = value;
                OnPropertyChanged();
            }
        }
        double? _fRColdPressure;
        public double? FRColdPressure
        {
            get
            {
                return _fRColdPressure;
            }
            set
            {
                if (_fRColdPressure == value)
                {
                    return;
                }

                _fRColdPressure = value;
                OnPropertyChanged();
            }
        }
        double? _rLColdPressure;
        public double? RLColdPressure
        {
            get
            {
                return _rLColdPressure;
            }
            set
            {
                if (_rLColdPressure == value)
                {
                    return;
                }

                _rLColdPressure = value;
                OnPropertyChanged();
            }
        }
        double? _rRColdPressure;
        public double? RRColdPressure
        {
            get
            {
                return _rRColdPressure;
            }
            set
            {
                if (_rRColdPressure == value)
                {
                    return;
                }

                _rRColdPressure = value;
                OnPropertyChanged();
            }
        }

        double? _fLCheckColdPressure;
        public double? FLCheckColdPressure
        {
            get
            {
                return _fLCheckColdPressure;
            }
            set
            {
                if (_fLCheckColdPressure == value)
                {
                    return;
                }

                _fLCheckColdPressure = value;
                OnPropertyChanged();
            }
        }
        double? _fRCheckColdPressure;
        public double? FRCheckColdPressure
        {
            get
            {
                return _fRCheckColdPressure;
            }
            set
            {
                if (_fRCheckColdPressure == value)
                {
                    return;
                }

                _fRCheckColdPressure = value;
                OnPropertyChanged();
            }
        }
        double? _rLCheckColdPressure;
        public double? RLCheckColdPressure
        {
            get
            {
                return _rLCheckColdPressure;
            }
            set
            {
                if (_rLCheckColdPressure == value)
                {
                    return;
                }

                _rLCheckColdPressure = value;
                OnPropertyChanged();
            }
        }
        double? _rRCheckColdPressure;
        public double? RRCheckColdPressure
        {
            get
            {
                return _rRCheckColdPressure;
            }
            set
            {
                if (_rRCheckColdPressure == value)
                {
                    return;
                }

                _rRCheckColdPressure = value;
                OnPropertyChanged();
            }
        }

        double? _fLBlktsandBleedPressure;
        public double? FLBlktsandBleedPressure
        {
            get
            {
                return _fLBlktsandBleedPressure;
            }
            set
            {
                if (_fLBlktsandBleedPressure == value)
                {
                    return;
                }

                _fLBlktsandBleedPressure = value;
                OnPropertyChanged();
            }
        }
        double? _fRBlktsandBleedPressure;
        public double? FRBlktsandBleedPressure
        {
            get
            {
                return _fRBlktsandBleedPressure;
            }
            set
            {
                if (_fRBlktsandBleedPressure == value)
                {
                    return;
                }

                _fRBlktsandBleedPressure = value;
                OnPropertyChanged();
            }
        }
        double? _rLBlktsandBleedPressure;
        public double? RLBlktsandBleedPressure
        {
            get
            {
                return _rLBlktsandBleedPressure;
            }
            set
            {
                if (_rLBlktsandBleedPressure == value)
                {
                    return;
                }

                _rLBlktsandBleedPressure = value;
                OnPropertyChanged();
            }
        }
        double? _rRBlktsandBleedPressure;
        public double? RRBlktsandBleedPressure
        {
            get
            {
                return _rRBlktsandBleedPressure;
            }
            set
            {
                if (_rRBlktsandBleedPressure == value)
                {
                    return;
                }

                _rRBlktsandBleedPressure = value;
                OnPropertyChanged();
            }
        }

        double? _fLRefBleedPressure;
        public double? FLRefBleedPressure
        {
            get
            {
                return _fLRefBleedPressure;
            }
            set
            {
                if (_fLRefBleedPressure == value)
                {
                    return;
                }

                _fLRefBleedPressure = value;
                OnPropertyChanged();
            }
        }
        double? _fRRefBleedPressure;
        public double? FRRefBleedPressure
        {
            get
            {
                return _fRRefBleedPressure;
            }
            set
            {
                if (_fRRefBleedPressure == value)
                {
                    return;
                }

                _fRRefBleedPressure = value;
                OnPropertyChanged();
            }
        }
        double? _rLRefBleedPressure;
        public double? RLRefBleedPressure
        {
            get
            {
                return _rLRefBleedPressure;
            }
            set
            {
                if (_rLRefBleedPressure == value)
                {
                    return;
                }

                _rLRefBleedPressure = value;
                OnPropertyChanged();
            }
        }
        double? _rRRefBleedPressure;
        public double? RRRefBleedPressure
        {
            get
            {
                return _rRRefBleedPressure;
            }
            set
            {
                if (_rRRefBleedPressure == value)
                {
                    return;
                }

                _rRRefBleedPressure = value;
                OnPropertyChanged();
            }
        }

        double? _fLNewRefPressure;
        public double? FLNewRefPressure
        {
            get
            {
                return _fLNewRefPressure;
            }
            set
            {
                if (_fLNewRefPressure == value)
                {
                    return;
                }

                _fLNewRefPressure = value;
                OnPropertyChanged();
            }
        }
        double? _fRNewRefPressure;
        public double? FRNewRefPressure
        {
            get
            {
                return _fRNewRefPressure;
            }
            set
            {
                if (_fRNewRefPressure == value)
                {
                    return;
                }

                _fRNewRefPressure = value;
                OnPropertyChanged();
            }
        }
        double? _rLNewRefPressure;
        public double? RLNewRefPressure
        {
            get
            {
                return _rLNewRefPressure;
            }
            set
            {
                if (_rLNewRefPressure == value)
                {
                    return;
                }

                _rLNewRefPressure = value;
                OnPropertyChanged();
            }
        }
        double? _rRNewRefPressure;
        public double? RRNewRefPressure
        {
            get
            {
                return _rRNewRefPressure;
            }
            set
            {
                if (_rRNewRefPressure == value)
                {
                    return;
                }

                _rRNewRefPressure = value;
                OnPropertyChanged();
            }
        }
        #endregion


        //COLD PRESSURE
        private void UpdateFLColdPressure() => UpdateColdPressure((double?)GetPropertyValue("FLReferencePressure"), ref _fLColdPressure, nameof(FLColdPressure));
        private void UpdateFRColdPressure() => UpdateColdPressure((double?)GetPropertyValue("FRReferencePressure"), ref _fRColdPressure, nameof(FRColdPressure));
        private void UpdateRLColdPressure() => UpdateColdPressure((double?)GetPropertyValue("RLReferencePressure"), ref _rLColdPressure, nameof(RLColdPressure));
        private void UpdateRRColdPressure() => UpdateColdPressure((double?)GetPropertyValue("RRReferencePressure"), ref _rRColdPressure, nameof(RRColdPressure));

        private void UpdateColdPressure(double? aRefPressure, ref double? ColdPressure, string aPropName)
        {
            var trackTemp = ((double?)GetPropertyValue("ColdTrackTemp")).Value;
            var rimTemp = ((double?)GetPropertyValue("ColdRimTemp")).Value;
            
            ColdPressure = Math.Round((double)(aRefPressure - (REFERENCE_RIM_TEMP - rimTemp) * (RIM_TEMP_COEF_Ax * aRefPressure + RIM_TEMP_COEF_B) +(REFERENCE_TRACK_TEMP - trackTemp) * TRACK_TEMP_COEF), 2);

            OnPropertyChanged(aPropName);
        }

        //CHECK COLD PRESSURE
        private void UpdateFLCheckColdPressure() => UpdateCheckColdPressure((double?)GetPropertyValue("FLReferencePressure"), (double?)GetPropertyValue("FLRimTemperature"), (double?)GetPropertyValue("FLOutofBlanketsPressure"), ref _fLCheckColdPressure, nameof(FLCheckColdPressure));
        private void UpdateFRCheckColdPressure() => UpdateCheckColdPressure((double?)GetPropertyValue("FRReferencePressure"), (double?)GetPropertyValue("FRRimTemperature"), (double?)GetPropertyValue("FROutofBlanketsPressure"), ref _fRCheckColdPressure, nameof(FRCheckColdPressure));
        private void UpdateRLCheckColdPressure() => UpdateCheckColdPressure((double?)GetPropertyValue("RLReferencePressure"), (double?)GetPropertyValue("RLRimTemperature"), (double?)GetPropertyValue("RLOutofBlanketsPressure"), ref _rLCheckColdPressure, nameof(RLCheckColdPressure));
        private void UpdateRRCheckColdPressure() => UpdateCheckColdPressure((double?)GetPropertyValue("RRReferencePressure"), (double?)GetPropertyValue("RRRimTemperature"), (double?)GetPropertyValue("RROutofBlanketsPressure"), ref _rRCheckColdPressure, nameof(RRCheckColdPressure));

        private void UpdateCheckColdPressure(double? aRefPressure, double? aRimTemperature, double? aOutofBlanketsPressure, ref double? CheckColdPressure, string aPropName)
        {
            var rimTemp = ((double?)GetPropertyValue("ColdRimTemp")).Value;

            CheckColdPressure = Math.Round((double)(aOutofBlanketsPressure - (aRimTemperature - rimTemp) * (RIM_TEMP_COEF_Ax * aRefPressure + RIM_TEMP_COEF_B)), 2);

            OnPropertyChanged(aPropName);
        }

        //DELTA PRESSURE
        private void UpdateDeltaPressure()
        {
            var trackTemp = ((double?)GetPropertyValue("ColdTrackTemp")).Value;
            var actualTrackTemp = ((double?)GetPropertyValue("ActualTrackTemp")).Value;

            DeltaPressure = Math.Round((double)(-(actualTrackTemp - trackTemp) * TRACK_TEMP_COEF / HOT_to_COLD_COEF), 2);

        }

        //BLANKET AND BLEED PRESSURE
        private void UpdateFLBlktsandBleedPressure() => UpdateBlktsandBleedPressure((double?)GetPropertyValue("FLOutofBlanketsPressure"), (double?)GetPropertyValue("FLHotBleed"), ref _fLBlktsandBleedPressure, nameof(FLBlktsandBleedPressure));
        private void UpdateFRBlktsandBleedPressure() => UpdateBlktsandBleedPressure((double?)GetPropertyValue("FROutofBlanketsPressure"), (double?)GetPropertyValue("FRHotBleed"), ref _fRBlktsandBleedPressure, nameof(FRBlktsandBleedPressure));
        private void UpdateRLBlktsandBleedPressure() => UpdateBlktsandBleedPressure((double?)GetPropertyValue("RLOutofBlanketsPressure"), (double?)GetPropertyValue("RLHotBleed"), ref _rLBlktsandBleedPressure, nameof(RLBlktsandBleedPressure));
        private void UpdateRRBlktsandBleedPressure() => UpdateBlktsandBleedPressure((double?)GetPropertyValue("RROutofBlanketsPressure"), (double?)GetPropertyValue("RRHotBleed"), ref _rRBlktsandBleedPressure, nameof(RRBlktsandBleedPressure));
        
        private void UpdateBlktsandBleedPressure(double? aOutofBlanketsPressure, double? aHotBleed, ref double? BlktsandBleedPressure, string aPropName)
        {
            if (DeltaPressure == null)
                BlktsandBleedPressure = null;
            else
                BlktsandBleedPressure = Math.Round((double)(aOutofBlanketsPressure + DeltaPressure + aHotBleed * (1 - HOT_BLEED_ERROR)), 2);

            OnPropertyChanged(aPropName);
        }

        //REF BLEED PRESSURE
        private void UpdateFLRefBleedPressure() => UpdateRefBleedPressure((double?)GetPropertyValue("FLReferencePressure"), (double?)GetPropertyValue("FLRimTemperature"), _fLBlktsandBleedPressure, ref _fLRefBleedPressure, nameof(FLRefBleedPressure));
        private void UpdateFRRefBleedPressure() => UpdateRefBleedPressure((double?)GetPropertyValue("FRReferencePressure"), (double?)GetPropertyValue("FRRimTemperature"), _fRBlktsandBleedPressure, ref _fRRefBleedPressure, nameof(FRRefBleedPressure));
        private void UpdateRLRefBleedPressure() => UpdateRefBleedPressure((double?)GetPropertyValue("RLReferencePressure"), (double?)GetPropertyValue("RLRimTemperature"), _rLBlktsandBleedPressure, ref _rLRefBleedPressure, nameof(RLRefBleedPressure));
        private void UpdateRRRefBleedPressure() => UpdateRefBleedPressure((double?)GetPropertyValue("RRReferencePressure"), (double?)GetPropertyValue("RRRimTemperature"), _rRBlktsandBleedPressure, ref _rRRefBleedPressure, nameof(RRRefBleedPressure));

        private void UpdateRefBleedPressure(double? aRefPressure, double? aRimTemperature, double? BlktsandBleedPressure, ref double? RefBleedPressure, string aPropName)
        {
            var actualTrackTemp = ((double?)GetPropertyValue("ActualTrackTemp")).Value;

            if (BlktsandBleedPressure == null)
                RefBleedPressure = null;
            else
                RefBleedPressure = Math.Round((double)((BlktsandBleedPressure - (aRimTemperature - TIRE_RUNNING_TEMP) * (RIM_TEMP_COEF_Ax * aRefPressure + RIM_TEMP_COEF_B))* HOT_to_COLD_COEF + (actualTrackTemp - REFERENCE_TRACK_TEMP) * TRACK_TEMP_COEF), 2);

            OnPropertyChanged(aPropName);
        }

        //NEW REF PRESSURE
        private void UpdateFLNewRefPressure() => UpdateNewRefPressure((double?)GetPropertyValue("FLReferencePressure"), (double?)GetPropertyValue("FrontTargetPressure"), (double?)GetPropertyValue("FLHotBleed"), (double?)GetPropertyValue("FLHotPressure"), ref _fLNewRefPressure, nameof(FLNewRefPressure));
        private void UpdateFRNewRefPressure() => UpdateNewRefPressure((double?)GetPropertyValue("FRReferencePressure"), (double?)GetPropertyValue("FrontTargetPressure"), (double?)GetPropertyValue("FRHotBleed"), (double?)GetPropertyValue("FRHotPressure"), ref _fRNewRefPressure, nameof(FRNewRefPressure));
        private void UpdateRLNewRefPressure() => UpdateNewRefPressure((double?)GetPropertyValue("RLReferencePressure"), (double?)GetPropertyValue("RearTargetPressure"), (double?)GetPropertyValue("RLHotBleed"), (double?)GetPropertyValue("RLHotPressure"), ref _rLNewRefPressure, nameof(RLNewRefPressure));
        private void UpdateRRNewRefPressure() => UpdateNewRefPressure((double?)GetPropertyValue("RRReferencePressure"), (double?)GetPropertyValue("RearTargetPressure"), (double?)GetPropertyValue("RRHotBleed"), (double?)GetPropertyValue("RRHotPressure"), ref _rRNewRefPressure, nameof(RRNewRefPressure));

        private void UpdateNewRefPressure(double? aRefPressure, double? aTarget, double? aHotBleed,double? aHotPressure, ref double? NewRefPressure, string aPropName)
        {
           NewRefPressure = Math.Round((double)(aRefPressure - (aHotPressure - aTarget) * HOT_to_COLD_COEF + (aHotBleed) * HOT_to_COLD_COEF), 2);

            OnPropertyChanged(aPropName);
        }


    }




}
