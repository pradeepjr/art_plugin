﻿using HHDev.DataManagement.Definitions.Models;
using HHDev.DataManagement.Desktop.Definitions.Displays;
using HHDev.Core.Sync.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ART.HHDM.DesktopPlugin.RunSheet
{
    public class ARTFlatLapModel : FlatLapModel
    {
        public ARTFlatLapModel(FlatLapModelInitializationObject aFlatLapModelInitializationObject) : base(aFlatLapModelInitializationObject)
        {

            //this.PropertyChanged += ARTFlatLapModel_PropertyChanged;
            //this.EntityPropertyChanged += ARTFlatLapModel_EntityPropertyChanged;

        }

        private void ARTFlatLapModel_EntityPropertyChanged(object sender, EntityPropertyUpdatedEventArgs e)
        {
            //if (e.Update.Name == nameof(FlatLapModel.LapTime))
            //    if (this.LapTime.HasValue)
            //        DemoCustomLapValue = this.LapTime.Value * 1.1;
            //    else
            //        DemoCustomLapValue = null;
        }

        private void ARTFlatLapModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            //if (e.PropertyName == nameof(FlatLapModel.LapTime))
            //    if (this.LapTime.HasValue)
            //        DemoCustomLapValue = this.LapTime.Value * 1.1;
            //    else
            //        DemoCustomLapValue = null;
        }

        private double? _lapsPossible;
        public double? LapsPossible
        {
            get
            {
                return _lapsPossible;
            }
            set
            {
                if (_lapsPossible == value)
                {
                    return;
                }

                _lapsPossible = value;
                OnPropertyChanged();
            }
        }

        private double? _remainingFuel;
        public double? RemainingFuel
        {
            get
            {
                return _remainingFuel;
            }
            set
            {
                if (_remainingFuel == value)
                {
                    return;
                }

                _remainingFuel = value;
                OnPropertyChanged();
            }
        }

        private double? _neededFuelCons;
        public double? NeededFuelCons
        {
            get
            {
                return _neededFuelCons;
            }
            set
            {
                if (_neededFuelCons == value)
                {
                    return;
                }

                _neededFuelCons = value;
                OnPropertyChanged();
            }
        }


        private double? _targetFuelCons;
        public double? TargetFuelCons
        {
            get
            {
                return _targetFuelCons;
            }
            set
            {
                if (_targetFuelCons == value)
                {
                    return;
                }

                _targetFuelCons = value;
                OnPropertyChanged();
            }
        }

    }
}
