﻿using HHDev.Core.NETStandard.Src;
using HHDev.DataManagement.Desktop.Definitions.Displays;
using ART.HHDM.DesktopPlugin.PressureCalculation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace ART.HHDM.DesktopPlugin.RunSheet
{
    public class ARTFlatRunSheetModel : FlatRunSheetModel
    {

        public ICommand SendHotPressuresToPressureCalculationCommand { get; set; }


        public ARTFlatRunSheetModel(FlatRunSheetModelInitializationObject aFlatRunSheetModelInitializationObject)
            : base(aFlatRunSheetModelInitializationObject)
        {
            SendHotPressuresToPressureCalculationCommand = new DelegateCommand(SendHotPressuresToPressureCalculation);

        }

        // this function is called each time the tyre set changes, so it can be used for things other than simply setting the cold pressures..
        public override void RefreshColdPressures()
        {
            if (TyreSet == null) return;

            // also want to copy the tyre specs to the setup sheet
            if (Setup != null)
            {
                Setup.SetPropertyValue("FlTyreCompound", TyreSet.FLTyre?.TyreSpecification?.Name);
                Setup.SetPropertyValue("FrTyreCompound", TyreSet.FRTyre?.TyreSpecification?.Name);
                Setup.SetPropertyValue("RlTyreCompound", TyreSet.RLTyre?.TyreSpecification?.Name);
                Setup.SetPropertyValue("RrTyreCompound", TyreSet.RRTyre?.TyreSpecification?.Name);
            }

            var pressureCalculation = (ARTPressureCalculationFlatModel)base.PressureCalculations.FirstOrDefault(x =>
            {
                var tyreSetId = (string)x.GetPropertyValue("TyreSetId");
                if (string.IsNullOrEmpty(tyreSetId)) return false;
                return tyreSetId == TyreSet.Id;
            });

            if (pressureCalculation == null)
            {
                base.RefreshColdPressures();
                return;
            }

            UpdateColdPressuresFromCalculation("FLColdPressure", pressureCalculation.FLColdPressure);
            UpdateColdPressuresFromCalculation("FRColdPressure", pressureCalculation.FRColdPressure);
            UpdateColdPressuresFromCalculation("RLColdPressure", pressureCalculation.RLColdPressure);
            UpdateColdPressuresFromCalculation("RRColdPressure", pressureCalculation.RRColdPressure);

            UpdateColdPressuresFromCalculation("FLTargetPressure", (double?)pressureCalculation.GetPropertyValue("FrontTargetPressure"));
            UpdateColdPressuresFromCalculation("FRTargetPressure", (double?)pressureCalculation.GetPropertyValue("FrontTargetPressure"));
            UpdateColdPressuresFromCalculation("RLTargetPressure", (double?)pressureCalculation.GetPropertyValue("RearTargetPressure"));
            UpdateColdPressuresFromCalculation("RRTargetPressure", (double?)pressureCalculation.GetPropertyValue("RearTargetPressure"));

            UpdateColdPressuresFromCalculation("FLHeatingTemp", (double?)pressureCalculation.GetPropertyValue("FrontOvenTemp"));
            UpdateColdPressuresFromCalculation("FRHeatingTemp", (double?)pressureCalculation.GetPropertyValue("FrontOvenTemp"));
            UpdateColdPressuresFromCalculation("RLHeatingTemp", (double?)pressureCalculation.GetPropertyValue("RearOvenTemp"));
            UpdateColdPressuresFromCalculation("RRHeatingTemp", (double?)pressureCalculation.GetPropertyValue("RearOvenTemp"));

        }

        private void SendHotPressuresToPressureCalculation()
        {
            if (TyreSet == null) return;

            var pressureCalculation = (ARTPressureCalculationFlatModel)base.PressureCalculations.FirstOrDefault(x =>
            {
                var tyreSetId = (string)x.GetPropertyValue("TyreSetId");
                if (string.IsNullOrEmpty(tyreSetId)) return false;
                return tyreSetId == TyreSet.Id;
            });

            if (pressureCalculation == null) return;

            pressureCalculation.SetPropertyValue("FLHotPressure", this.GetPropertyValue("FLHotPressure"));
            pressureCalculation.SetPropertyValue("FRHotPressure", this.GetPropertyValue("FRHotPressure"));
            pressureCalculation.SetPropertyValue("RLHotPressure", this.GetPropertyValue("RLHotPressure"));
            pressureCalculation.SetPropertyValue("RRHotPressure", this.GetPropertyValue("RRHotPressure"));

            pressureCalculation.SetPropertyValue("FLHotBleed", this.GetPropertyValue("FLOnCarBleed"));
            pressureCalculation.SetPropertyValue("FRHotBleed", this.GetPropertyValue("FROnCarBleed"));
            pressureCalculation.SetPropertyValue("RLHotBleed", this.GetPropertyValue("RLOnCarBleed"));
            pressureCalculation.SetPropertyValue("RRHotBleed", this.GetPropertyValue("RROnCarBleed"));
            if (this.GetPropertyValue("FLOnCarBleed") == null) pressureCalculation.SetPropertyValue("FLHotBleed", 0);
            if (this.GetPropertyValue("FROnCarBleed") == null) pressureCalculation.SetPropertyValue("FRHotBleed", 0);
            if (this.GetPropertyValue("RLOnCarBleed") == null) pressureCalculation.SetPropertyValue("RLHotBleed", 0);
            if (this.GetPropertyValue("RROnCarBleed") == null) pressureCalculation.SetPropertyValue("RRHotBleed", 0);

            pressureCalculation.SetPropertyValue("FLOutofBlanketsPressure", this.GetPropertyValue("FLOnCarPressure"));
            pressureCalculation.SetPropertyValue("FROutofBlanketsPressure", this.GetPropertyValue("FROnCarPressure"));
            pressureCalculation.SetPropertyValue("RLOutofBlanketsPressure", this.GetPropertyValue("RLOnCarPressure"));
            pressureCalculation.SetPropertyValue("RROutofBlanketsPressure", this.GetPropertyValue("RROnCarPressure"));

            pressureCalculation.SetPropertyValue("FLRimTemperature", this.GetPropertyValue("FLOnCarTemp"));
            pressureCalculation.SetPropertyValue("FRRimTemperature", this.GetPropertyValue("FROnCarTemp"));
            pressureCalculation.SetPropertyValue("RLRimTemperature", this.GetPropertyValue("RLOnCarTemp"));
            pressureCalculation.SetPropertyValue("RRRimTemperature", this.GetPropertyValue("RROnCarTemp"));
        }

        private void UpdateColdPressuresFromCalculation(string aRunSheetParameterName, double? aCalculationValue)
        {
            var flCold = (double?)this.GetPropertyValue(aRunSheetParameterName);
            if (aCalculationValue.HasValue && (!flCold.HasValue || aCalculationValue != flCold.Value))
                SetPropertyValue(aRunSheetParameterName, aCalculationValue);

        }



        protected override void UpdateRunStats()
        {
            try
            {
                base.UpdateRunStats();

                var laps = base.Laps.Where(x =>
                {
                    return x.IncludeInCalculation && x.Marker != HHDev.Core.NETStandard.Enums.eLapMarker.FCY && x.Marker != HHDev.Core.NETStandard.Enums.eLapMarker.OutIn && x.Marker != HHDev.Core.NETStandard.Enums.eLapMarker.Out && x.Marker != HHDev.Core.NETStandard.Enums.eLapMarker.In && x.FuelConsumed.HasValue;
                });

                if (laps.Count() == 00)
                {
                    base.SetPropertyValue("AvgGreenFuelCons", null);
                    return;
                }

                var avg = laps.Average(x => x.FuelConsumed);
                base.SetPropertyValue("AvgGreenFuelCons", avg);

                var laps2 = base.Laps.Where(x =>
                {
                    return x.FuelConsumed.HasValue;
                });

                if (laps2.Count() == 00)
                {
                    base.SetPropertyValue("TotalCons", null);
                    return;
                }

                var total = laps2.Sum(x => x.FuelConsumed);
                base.SetPropertyValue("TotalCons", total);


                var Fuel_remaining = (base.Laps.Where(x => { return x.FuelConsumed.HasValue; }).Select(x => x.FuelRemaining)).Last();             
                if (Fuel_remaining != null && avg != null) base.SetPropertyValue("MaxLap", Fuel_remaining / avg);

                            

                foreach (var lap in base.Laps)
                {
                    if (lap.FuelRemaining.HasValue)
                    {
                        ((ARTFlatLapModel)lap).LapsPossible = Math.Round((double)((lap.FuelRemaining / avg) + lap.OutingLaps), 1);
                        ((ARTFlatLapModel)lap).RemainingFuel = Math.Round((double)(lap.FuelRemaining - ((int)(lap.FuelRemaining / avg)) * avg), 1);
                        ((ARTFlatLapModel)lap).NeededFuelCons = Math.Round((double)(lap.FuelRemaining / (int)(lap.FuelRemaining / avg)), 1);

                        if (base.Doubles.GetPropertyValue("TargetLaps") != null)
                            ((ARTFlatLapModel)lap).TargetFuelCons = Math.Round((double)(lap.FuelRemaining / ((int)base.Doubles.GetPropertyValue("TargetLaps") - lap.OutingLaps)), 1);
                    }
                    //else
                    //    ((ARTFlatLapModel)lap).LapsPossible = null;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }



        }
    }
}
