﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ART.HHDM.DesktopPlugin.RunSheet
{
    /// <summary>
    /// Interaction logic for HelpSheet1View.xaml
    /// </summary>
    public partial class HelpSheet1View : Window
    {
        public HelpSheet1View()
        {
            InitializeComponent();
        }

        public HelpSheet1View(string Image_name) : this()
        {
            try
            {
                if (Image_name == "Multi_Switch")
                {
                    this.Runsheet_img.Source = new BitmapImage(new Uri("/ART.HHDM.DesktopPlugin;component/Ressources/Multi_Switch.JPG", UriKind.Relative));
                }
                else if (Image_name == "RSP")
                {
                    this.Runsheet_img.Source = new BitmapImage(new Uri("/ART.HHDM.DesktopPlugin;component/Ressources/RSP.JPG", UriKind.Relative));
                }
                else if (Image_name == "Steering")
                {
                    this.Runsheet_img.Source = new BitmapImage(new Uri("/ART.HHDM.DesktopPlugin;component/Ressources/Steering.JPG", UriKind.Relative));
                }
                else if (Image_name == "Track Map")
                {
                    this.Runsheet_img.Source = new BitmapImage(new Uri("/ART.HHDM.DesktopPlugin;component/Ressources/Track Map.JPG", UriKind.Relative));
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.ToString());
            }
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

    }
}
