﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ART.HHDM.DesktopPlugin.RunSheet
{
    /// <summary>
    /// Interaction logic for RunSheetRightPanel.xaml
    /// </summary>
    public partial class RunSheetRightPanel : UserControl
    {
        public RunSheetRightPanel()
        {
            InitializeComponent();
        }
        private void Multi_Switch_btn_Clicked(object sender, RoutedEventArgs e)
        {
            HelpSheet1View helpsheet1 = new HelpSheet1View("Multi_Switch");
            helpsheet1.Show();
        }

        private void RSP_btn_Clicked(object sender, RoutedEventArgs e)
        {
            HelpSheet1View helpsheet1 = new HelpSheet1View("RSP");
            helpsheet1.Show();
        }

        private void Steering_btn_Clicked(object sender, RoutedEventArgs e)
        {
            HelpSheet1View helpsheet1 = new HelpSheet1View("Steering");
            helpsheet1.Show();
        }

        private void Track_Map_btn_Clicked(object sender, RoutedEventArgs e)
        {
            HelpSheet1View helpsheet1 = new HelpSheet1View("Track Map");
            helpsheet1.Show();
        }

       
    }
}
