﻿using HHDev.Core.NETStandard.Enums;
using HHDev.DataManagement.Definitions.Entities;
using HHDev.DataManagement.Definitions.Models;
using HHDev.DataManagement.Desktop.Definitions.Displays;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;

namespace ART.HHDM.DesktopPlugin.SetupSheet
{
    public class ARTFlatSetupSheetModel : FlatSetupSheetModel
    {

       

        double? frontWeightDelta;
        #region Calculations
        public double? FrontWeightDelta
        {
            get
            {
                return frontWeightDelta;
            }
            set
            {
                if (frontWeightDelta == value)
                {
                    return;
                }

                frontWeightDelta = value;
                OnPropertyChanged();
            }
        }
        double? rearWeightDelta;
        public double? RearWeightDelta
        {
            get
            {
                return rearWeightDelta;
            }
            set
            {
                if (rearWeightDelta == value)
                {
                    return;
                }

                rearWeightDelta = value;
                OnPropertyChanged();
            }
        }
        double? crossWeightDelta;
        public double? CrossWeightDelta
        {
            get
            {
                return crossWeightDelta;
            }
            set
            {
                if (crossWeightDelta == value)
                {
                    return;
                }

                crossWeightDelta = value;
                OnPropertyChanged();
            }
        }
        double? frontWeightPercent;
        public double? FrontWeightPercent
        {
            get
            {
                return frontWeightPercent;
            }
            set
            {
                if (frontWeightPercent == value)
                {
                    return;
                }

                frontWeightPercent = value;
                OnPropertyChanged();
            }
        }

        double? crossWeightPercent;
        public double? CrossWeightPercent
        {
            get
            {
                return crossWeightPercent;
            }
            set
            {
                if (crossWeightPercent == value)
                {
                    return;
                }

                crossWeightPercent = value;
                OnPropertyChanged();
            }
        }

        double? totalWeight;
        public double? TotalWeight
        {
            get
            {
                return totalWeight;
            }
            set
            {
                if (totalWeight == value)
                {
                    return;
                }

                totalWeight = value;
                OnPropertyChanged();
            }
        }
        #endregion

        #region Dropdown Values
        public BindingList<string> DropGears { get; set; }
        public BindingList<string> TorsionBars { get; set; }
        public BindingList<string> SpringRates { get; set; }
        public BindingList<string> FrontLatDampers { get; set; }
        public BindingList<string> RearLatDampers { get; set; }
        public BindingList<string> FrontThirdElementDampers { get; set; }
        public BindingList<string> RearThirdElementDampers { get; set; }
        public BindingList<string> FrontAntiRollBars { get; set; }
        public BindingList<string> RearAntiRollBars { get; set; }
        public BindingList<string> AeroConfig { get; set; }
        public BindingList<string> Gear1 { get; set; }
        public BindingList<string> Gear2 { get; set; }
        public BindingList<string> Gear3 { get; set; }
        public BindingList<string> Gear4 { get; set; }
        public BindingList<string> Gear5 { get; set; }
        public BindingList<string> Gear6 { get; set; }
        public BindingList<string> BevelGears { get; set; }
        public BindingList<string> FinalDriveGears { get; set; }
        public BindingList<string> Blankings { get; set; }
        public BindingList<string> FrontFlaps { get; set; }
        public BindingList<string> FrontFWAPanels { get; set; }
        public BindingList<string> FrontHoods { get; set; }
        public BindingList<string> FrontFlicks { get; set; }
        public BindingList<double?> RearWings { get; set; }
        public BindingList<string> RearWingGurneys { get; set; }
        public BindingList<string> RearFloorGurneys { get; set; }
        public BindingList<double> RearBonnetGurneys { get; set; }
        public BindingList<string> RWGurneys { get; set; }
        public BindingList<string> RWArchRamps { get; set; }
        #endregion

        public ARTFlatSetupSheetModel(FlatSetupSheetModelInitializationObject anInitObject)
            : base(anInitObject)
        {
            InitDropdownValues();
            base.RegisterParameterAndFunction(new string[] { "FLWeight", "FRWeight", "RLWeight", "RRWeight" }, UpdateWeightCalcs,
                                              new string[] { nameof(FrontWeightDelta), nameof(RearWeightDelta), nameof(CrossWeightDelta), nameof(FrontWeightPercent), nameof(TotalWeight), nameof(CrossWeightPercent) });
                       
        }

       
        private void UpdateWeightCalcs()
        {
            double? fLWeight = Doubles.GetPropertyValue("FLWeight");
            double? fRWeight = Doubles.GetPropertyValue("FRWeight");
            double? rLWeight = Doubles.GetPropertyValue("RLWeight");
            double? rRWeight = Doubles.GetPropertyValue("RRWeight");
                       
                FrontWeightDelta = Math.Round((double)Math.Abs(Math.Abs(fLWeight.Value) - Math.Abs(fRWeight.Value)),2);
                RearWeightDelta = Math.Round((double)Math.Abs(Math.Abs(rLWeight.Value) - Math.Abs(rRWeight.Value)),2);
                        
                var fLrR = Math.Abs(Math.Abs(fLWeight.Value) - Math.Abs(rRWeight.Value));
                var fRrL = Math.Abs(Math.Abs(fRWeight.Value) - Math.Abs(rLWeight.Value));

                CrossWeightDelta = Math.Abs(fLrR - fRrL);
                TotalWeight = fLWeight+fRWeight+rLWeight+rRWeight;
                FrontWeightPercent = Math.Round((double)(((fLWeight.Value + fRWeight.Value) / TotalWeight) * 100), 2);
                CrossWeightPercent = Math.Round((double)(((fLWeight.Value + rRWeight.Value) / TotalWeight) * 100), 2);


        }

        private void InitDropdownValues()
        {
            TorsionBars = new BindingList<string>() { "M300-16.65", "M300-16.44", "M300-16.21", "M300-15.98", "M300-15.74", "M350-15.48", "M350-15.21", "M350-14.93", "M350-14.64", "M350-14.34", "M350-14.03", "M350-13.71" };
            SpringRates = new BindingList<string>() { "1500", "1450", "1400", "1350", "1300", "1250", "1200", "1150", "1100", "1050", "1000", "950", "900", "850", "800", "750", "700", "650", "600" };
            FrontLatDampers = new BindingList<string>() { "PKM-P30", "PKM-P1755047" };
            RearLatDampers = new BindingList<string>() { "PKM-P1755047" };
            FrontThirdElementDampers = new BindingList<string>() { "PKM-P11837002" };
            RearThirdElementDampers = new BindingList<string>() { "PKM-P11845075" };
            FrontAntiRollBars = new BindingList<string>() { "18x1.5 sft", "18x1.5 stiff", "28x1.5 stiff", "Bar18x1 5_softBlade", "Bar18x1 5_stiffBlade", "Bar28x1 5_softBlade", "Bar28x1 5_stiffBlade", "Off"};
            RearAntiRollBars = new BindingList<string>() { "14_D1", "14_D2", "14_D3", "bar13solid_blade2_P1", "bar14solid_blade1_P1", "bar14solid_blade1_P2", "bar14solid_blade1_P3", "Off"};
            AeroConfig = new BindingList<string>() { "LDF", "HDF" };
            Gear1 = new BindingList<string>() { "13/37", "13/35","13/36","13/34","14/36" };
            Gear2 = new BindingList<string>() { "14/33","15/33", "16/33","16/32","16/31" };
            Gear3 = new BindingList<string>() { "16/31","16/30","17/31","15/27", "18/31","16/27","16/26","19/30","18/28" };
            Gear4 = new BindingList<string>() { "17/30","18/31","16/27","16/26","19/30","18/28","17/26","19/28","19/27","18/25","20/27","19/25" };
            Gear5 = new BindingList<string>() { "18/28","17/26","19/28","19/27","18/25","20/27","19/25","20/26","22/28","21/26","19/23","21/25","23/27","20/23","23/26","19/21" };
            Gear6 = new BindingList<string>() { "19/27","18/25","20/27","19/25","20/26","22/28","21/26","19/23","21/25","23/27","20/23","23/26","19/21","23/25","19/20","23/24","24/24","24/23","27/25","22/20" };
            BevelGears = new BindingList<string>() { "23/20" };
            FinalDriveGears = new BindingList<string>() { "15/47", "16/45" };
            DropGears = new BindingList<string>() { "19/24*13/46","23/21*13/46","23/20*13/46","19/24*13/43","23/21*13/43","23/20*13/43","19/24*15/47","23/21*15/47","23/20*15/47","19/24*16/45","23/21*16/45","23/20*16/45" };
            FrontFlaps = new BindingList<string>() { "P1", "P2", "P3", "P4", "P5" };
            FrontFWAPanels = new BindingList<string>() { "HDF STD", "MDF", "HDF Opt1", "HDF Op2","LDF STD","LDF OPT"};
            FrontHoods = new BindingList<string>() { "Fully Open", "Mid Open", "Closed" };
            FrontFlicks = new BindingList<string>() { "Double", "Only Lower", "Without" };
            RearWings = new BindingList<double?>() { -1,0,1,2,3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,21 };
            //RearWingGurneys = new BindingList<string>() { "On", "Off" };
            RearFloorGurneys = new BindingList<string>() { "On", "Off" };
            RearBonnetGurneys = new BindingList<double>() {  8,20,30,40,50 };
            RWGurneys = new BindingList<string>() { "5*1200 mm", "5 full Span", "Off" };
            RWArchRamps = new BindingList<string>() { "On", "Off"};
        }


        public static void ExportSetups(IEnumerable<FlatSetupSheetModel> aSetups, CustomPropertyDefinition aDefinition)
        {
            SaveFileDialog dlg = new SaveFileDialog();

            dlg.FileName = "Setups";
            dlg.DefaultExt = ".csv";
            dlg.Filter = "Csv Documents (.csv)|*.csv";

            if (dlg.ShowDialog() != true)
                return;

            var csv = GetCSV(aSetups.ToList(), aDefinition);

            File.WriteAllText(dlg.FileName, csv);

            Process.Start(dlg.FileName);
        }

        public static string GetCSV(List<FlatSetupSheetModel> aFlatSetupSheetModels, CustomPropertyDefinition aDefinition)
        {
            if (aFlatSetupSheetModels == null || aFlatSetupSheetModels.Count == 0)
                return null;

            var csv = new StringBuilder();

            var headers = aDefinition.CustomPropertyDescriptions.Select(p => p.Name).ToList();

            headers.Insert(0, "Name");

            csv.AppendLine(string.Join(",", headers));

            foreach (var setup in aFlatSetupSheetModels)
            {
                var newLine = "";

                foreach (var header in headers)
                {
                    if (header == "Name")
                    {
                        newLine += setup.Name;
                    }
                    else
                    {
                        var propInfo = aDefinition.CustomPropLookup[header];

                        if (propInfo.PropertyType == ePropertyType.Text)
                        {
                            var value = $"\"{setup.Strings.GetPropertyValue(header)?.Replace("\"", "\"\"")}\",";
                            newLine += value.Replace("\r\n", "");
                        }

                        else if (propInfo.PropertyType == ePropertyType.Double)
                            newLine += $"\"{setup.Doubles.GetPropertyValue(header)?.ToString(CultureInfo.InvariantCulture)}\",";

                        else if (propInfo.PropertyType == ePropertyType.Boolean)
                        {
                            var boolVal = setup.Booleans.GetPropertyValue(header) ? "1" : "0";
                            newLine += $"\"{boolVal}\",";
                        }
                    }
                }

                newLine = newLine.Remove(newLine.Length - 1);
                csv.AppendLine(newLine);
            }

            return csv.ToString();
        }
    }
}
