﻿using System;
using System.Text.RegularExpressions;
using System.Windows.Controls;

namespace ART.HHDM.DesktopPlugin.SetupSheet
{
    /// <summary>
    /// Interaction logic for ARTSetupSheetDesignView.xaml
    /// </summary>
    /// 
    public partial class ARTSetupSheetDesignView : ContentControl
    {
        public ARTSetupSheetDesignView()
        {
            InitializeComponent();
        }
    }
}