﻿using System;
using System.Text.RegularExpressions;
using System.Windows.Controls;

namespace ART.HHDM.DesktopPlugin.SetupSheet
{
    public partial class ARTSetupSheetEditorView : UserControl
    {
        public ARTSetupSheetEditorView()
        {
            InitializeComponent();
        }

        private void RestrictNumeric(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }

        private bool IsTextAllowed(string text)
        {
            var match = Regex.IsMatch(text, @"^-?\d*([\,\.]\d*)?$");
            return match;
        }

        private void PasteRestrictNumeric(object sender, System.Windows.DataObjectPastingEventArgs e)
        {
            var text = e.DataObject.GetData(typeof(string));

            if (!IsTextAllowed(text as string))
                e.CancelCommand();
        }

        private void AllowAnyInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            e.Handled = false;
        }

        private void AllowAnyPaste(object sender, System.Windows.DataObjectPastingEventArgs e)
        {

        }
    }
}