﻿using HHDev.DataManagement.Definitions.Entities;
using HHDev.DataManagement.Desktop.Definitions.Displays;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ART.HHDM.DesktopPlugin.TyreSpec
{
    public class FlatTyreSpecModel : FlatTyreSpecificationModel
    {
    

        public FlatTyreSpecModel(TyreSpecification anEntity, CustomPropertyDefinition aDefinition) : base(anEntity, aDefinition)
        {

        }
    }
}
