﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace ART.HHDM.DesktopPlugin.TyreSpec
{
    public class BaseSpecItem
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public BaseSpecItem(string anId, string aName)
        {
            Id = anId;
            Name = aName;
        }
    }
}
